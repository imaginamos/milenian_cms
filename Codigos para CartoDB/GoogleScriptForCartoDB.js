/*This code must be included in Google Scripts*/

//Function for open Google Spreadsheet and clear for insertion
//This function is the main function and this will call to the other functions
function prepareSpreadSheet(){
  
  //Create a Spread Sheet and get the URL
  var urlSS = 'https://docs.google.com/spreadsheets/d/1KKJu03aUkP0-2i8YKw02OquNogG-QTbB14jrN2TFisc/edit?usp=sharing';
  
  var ss = SpreadsheetApp.openByUrl(urlSS);  
  SpreadsheetApp.setActiveSpreadsheet(ss);  
  
  var activeSS = SpreadsheetApp.getActiveSpreadsheet();   
  var nameSS = activeSS.getSheetByName(ss.getSheetName());
  nameSS.clear();
  
  //Connect to data base
  var connection = connectToDB();
  
  //Edit the query for get records from DB  
  var stringQuery = 'SELECT c.firstname,c.lastname,a.latitude,a.lengths'
  +' FROM ps_customer c, ps_address a' 
  +' WHERE a.id_customer = c.id_customer AND a.latitude != "" AND a.lengths != "";' 
  
  var queryResult = query(stringQuery, connection);
  
  //edit values for Spread Sheet header  
  var arrayHeader = ["firstname" ,"lastname", "latitude", "longitude"];
  
  //edit values for get records from database  
  var arrayFields = ["firstname", "lastname", "latitude", "lengths"];
  
  writeSpreadSheet(arrayHeader, nameSS, queryResult, arrayFields);  
  
}

//Function for connect to any database
function connectToDB() {
  
  // Replace the variables in this block with real values.
  var address = '45.33.116.10';
  var user = 'imaginamos';
  var userPwd = '123456';
  var db = 'mileniamdb';

  var dbUrl = 'jdbc:mysql://' + address + '/' + db;
  
  var conn = Jdbc.getConnection(dbUrl, user, userPwd);

  return conn;  
}

//Function for get records from DB

function query(stringQuery, conn){
  
  var stmt = conn.createStatement();
  var results = stmt.executeQuery(stringQuery);
  return results;  
}

//Function for write Spread Sheet

function writeSpreadSheet(arrayHeader, ss, queryResult, arrayFields){
  
  var start = new Date();
  
  //add the header to the Spread Sheet
  ss.appendRow(arrayHeader);

  // add the rows to the Spread Sheet
  while (queryResult.next()) {       
        
    ss.appendRow([queryResult.getString(arrayFields[0]), 
                  queryResult.getString(arrayFields[1]),
                  queryResult.getString(arrayFields[2]).replace(".",","), 
                  queryResult.getString(arrayFields[3]).replace(".",",")]);
  }

  queryResult.close();

  var end = new Date();
  Logger.log('Time elapsed: %sms', end - start);
}